# Sample Hugo

This project is used for experimenting with Hugo. Part of it will utilise GitLab's CI/CD features.

See `.gitlab-ci.yml` for the GitLab CI/CD setup. In addition to the code in this repository, a GitLab runner works with a Docker executor to run all the steps. The deploy-job will deploy the artifacts to a remote server as defined by variables that are set.

